mixin MagicalSkill {
  int heal45() {
    return 45;
  }

  int heal30() {
    return 30;
  }

  int plus5Damage(int playerDamage) {
    return playerDamage + 5;
  }
}
