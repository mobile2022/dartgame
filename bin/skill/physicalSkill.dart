mixin PhysicalSkill {
  int doubleDamage(int playerDamage) {
    return playerDamage * 2;
  }

  int plus10Damage(int playerDamage) {
    return playerDamage + 10;
  }

  int heal10() {
    return 10;
  }

  // int threeSecond();
  // String showTime();
  // int bleedOut();
}
