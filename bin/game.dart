import 'dart:io';
import 'dart:math';

import 'package:game/game.dart' as game;

import 'character/archer.dart';
import 'character/cowboy.dart';
import 'character/elf.dart';
import 'character/wizard.dart';
import 'model/warrior.dart';

import 'package:chalkdart/chalk.dart';

class RunGame {
  Warrior? player1;
  Warrior? player2;
  String? p1SkillNum = '';
  String? p2SkillNum = '';

  var winner = null;

  RunGame() {
    this.player1;
    this.player2;
    print("\x1B[2J\x1B[0;0H");
  }

  void showWelcome() {
    print(chalk.redBright(
        '''

                      ███████╗██████╗ ███████╗███████╗██████╗     ███████╗██╗ ██████╗ ██╗  ██╗████████╗███████╗██████╗ 
                      ██╔════╝██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔════╝██║██╔════╝ ██║  ██║╚══██╔══╝██╔════╝██╔══██╗
                      ███████╗██████╔╝█████╗  █████╗  ██║  ██║    █████╗  ██║██║  ███╗███████║   ██║   █████╗  ██████╔╝
                      ╚════██║██╔═══╝ ██╔══╝  ██╔══╝  ██║  ██║    ██╔══╝  ██║██║   ██║██╔══██║   ██║   ██╔══╝  ██╔══██╗
                      ███████║██║     ███████╗███████╗██████╔╝    ██║     ██║╚██████╔╝██║  ██║   ██║   ███████╗██║  ██║
                      ╚══════╝╚═╝     ╚══════╝╚══════╝╚═════╝     ╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝
                      
'''));
  }

  void mainMenu() {
    showWelcome();
    stdout.write(chalk.bold.white('>> 1 '));
    print(chalk.bold.green('PLAY'));
    stdout.write(chalk.bold.white('>> 2 '));
    print(chalk.bold.red('EXIT'));
  }

  bool enterGame() {
    int select = 0;

    do {
      String? choice = stdin.readLineSync();
      if (choice == '1') {
        return true;
      } else if (choice == '2') {
        return false;
      } else {
        print("\x1B[2J\x1B[0;0H"); // clear entire screen, move cursor to 0;0
        mainMenu();
      }
    } while (select == 0);

    return false;
  }

  void selectWarriorScreen() {
    showSelectWarriorScreen(' PLAYER 1 ', 1);
    selectWarrior(1);
    showSelectWarriorScreen(' PLAYER 2 ', 2);
    selectWarrior(2);
    print("\x1B[2J\x1B[0;0H");

    // print(player1!.name); // Test if select warrior complete
    // print(player2!.name); // Test if select warrior complete
  }

  void selectWarrior(int player) {
    bool ps = true;
    do {
      String? p = stdin.readLineSync();
      if (player == 1) {
        if (p == '1') {
          player1 = Cowboy();
          ps = true;
        } else if (p == '2') {
          player1 = Archer();
          ps = true;
        } else if (p == '3') {
          player1 = Elf();
          ps = true;
        } else if (p == '4') {
          player1 = Wizard();
          ps = true;
        } else {
          ps = false;
          print("\x1B[2J\x1B[0;0H");
          showSelectWarriorScreen(' PLAYER 1 ', 1);
        }
      }
      if (player == 2) {
        if (p == '1') {
          player2 = Cowboy();
          ps = true;
        } else if (p == '2') {
          player2 = Archer();
          ps = true;
        } else if (p == '3') {
          player2 = Elf();
          ps = true;
        } else if (p == '4') {
          player2 = Wizard();
          ps = true;
        } else {
          ps = false;
          print("\x1B[2J\x1B[0;0H");
          showSelectWarriorScreen(' PLAYER 2 ', 2);
        }
      }
    } while (ps == false);
  }

  void showSelectWarriorScreen(String player, int playerColor) {
    print("\x1B[2J\x1B[0;0H");
    print(chalk.bold.green(
        '''
     __  __      __  __ ___              __   __     __   __  
    (_  |_  |   |_  /    |    |  |  /\\  |__) |__) | /  \\ |__) 
    __) |__ |__ |__ \\__  |    |/\\| /--\\ | \\  | \\  | \\__/ | \\  
                                                                                                                              
    '''));
    if (playerColor == 1) {
      print(chalk.bold.onBlue.white(player));
      print(chalk.bold.cyanBright(
          ''' 
    
>>> 1 COWBOY 
>>> 2 ARCHER
>>> 3 ELF
>>> 4 WIZARD
    
    '''));
    } else if (playerColor == 2) {
      print(chalk.bold.onRed.white(player));
      print(chalk.bold.cyanBright(
          ''' 
    
>>> 1 COWBOY 
>>> 2 ARCHER
>>> 3 ELF
>>> 4 WIZARD

    '''));
    }
  }

  double timing(Warrior player) {
    print("\x1B[2J\x1B[0;0H");

    Stopwatch stopwatch = Stopwatch();
    double timeDiff = 0;
    int warriorTime = player.maxCountdown;

    var randomTime = Random().nextInt(warriorTime);
    print(chalk.yellow('===================='));
    stdout.write(chalk.whiteBright('YOU\'VE GOT '));
    stdout.write(chalk.bold.onBlack.redBright(randomTime));
    print(chalk.whiteBright(' SECONDS'));
    print(chalk.yellow('====================\n'));
    sleep(Duration(seconds: 3));

    // print(chalk.white('PRESS ENTER TO START'));
    // String? t0 = stdin.readLineSync();
    print(chalk.bold.greenBright('- START IN 3 SECOND -'));
    sleep(Duration(seconds: 3));
    print(chalk.bold.greenBright('-- GO! --\n'));
    print('PRESS ENTER TO STOP');
    stopwatch.start();
    String? t1 = stdin.readLineSync();
    stopwatch.stop();
    print(
        'You press in ${(stopwatch.elapsedMilliseconds * 0.001).toStringAsFixed(4)}');

    timeDiff = (randomTime - (stopwatch.elapsedMilliseconds * 0.001)).abs();
    sleep(Duration(seconds: 3));
    stopwatch.reset();
    print("\x1B[2J\x1B[0;0H");

    return timeDiff;
  }

  void ingame() {
    double p1TimeDiff;
    double p2TimeDiff;
    Stopwatch stopwatch = Stopwatch();

    while (winner == null) {
      print(chalk.bold.blue('===== PLAYER 1 TURN =====\n\n'));
      showPlayerStat(player1!);
      p1SkillNum = stdin.readLineSync();
      p1TimeDiff = timing(player1!);
      print(chalk.bold.red('===== PLAYER 2 TURN =====\n\n'));
      showPlayerStat(player2!);
      p2SkillNum = stdin.readLineSync();
      p2TimeDiff = timing(player2!);

      showRoundResult(p1TimeDiff, p2TimeDiff);
      p1SkillNum = '';
      p2SkillNum = '';
    }

    showWinner(winner);
  }

  void showRoundResult(double p1TimeDiff, double p2TimeDiff) {
    print('PLAYER 1 MISS BY ${p1TimeDiff.toStringAsFixed(4)} SECOND');
    print('PLAYER 2 MISS BY ${p2TimeDiff.toStringAsFixed(4)} SECOND\n\n');

    if (p1TimeDiff < p2TimeDiff) {
      print('PLAYER 1 WIN THIS ROUND !!!\n');
      player1!.attack();
      if (p1SkillNum == '' || p1SkillNum == '1') {
        print('PLAYER 1 HP : ${player1!.health}');
        print(
            'PLAYER 2 HP : ${player2!.health} - ${player1!.useSkill(p1SkillNum)}\n\n');
        player2!.health = player2!.health - player1!.useSkill(p1SkillNum);
      } else if (p1SkillNum == '2') {
        print(
            'PLAYER 1 HP : ${player1!.health} + ${player1!.useSkill(p1SkillNum)}');
        print('PLAYER 2 HP : ${player2!.health}\n\n');
        player1!.health = player1!.health + player1!.useSkill(p1SkillNum);
      }
    } else {
      print('PLAYER 2 WIN THIS ROUND !!!\n');
      player2!.attack();
      if (p2SkillNum == '' || p2SkillNum == '1') {
        print(
            'PLAYER 1 HP : ${player1!.health}- ${player2!.useSkill(p2SkillNum)}');
        print('PLAYER 2 HP : ${player2!.health}\n\n');
        player1!.health = player1!.health - player2!.useSkill(p2SkillNum);
      } else if (p2SkillNum == '2') {
        print('PLAYER 1 HP : ${player1!.health}');
        print(
            'PLAYER 2 HP : ${player2!.health} + ${player2!.useSkill(p2SkillNum)}\n\n');
        player2!.health = player2!.health + player2!.useSkill(p2SkillNum);
      }
      // print(
      //     'PLAYER 1 HP : ${player1!.health}- ${player2!.useSkill(p2SkillNum)}');
      // print('PLAYER 2 HP : ${player2!.health}\n\n');
      // player1!.health = player1!.health - player2!.useSkill(p2SkillNum);

    }

    if (p1SkillNum != '' &&
        (int.parse(p1SkillNum!) >= 1 && int.parse(p1SkillNum!) <= 2)) {
      player1!.skillList[int.parse(p1SkillNum!) - 1] = ' - ';
    }
    if (p2SkillNum != '' &&
        (int.parse(p2SkillNum!) >= 1 && int.parse(p2SkillNum!) <= 2)) {
      player2!.skillList[int.parse(p2SkillNum!) - 1] = ' - ';
    }

    // print(player1!.health);
    // print(player2!.health);

    if (player1!.health <= 0) {
      winner = player2;
    } else if (player2!.health <= 0) {
      winner = player1;
    }

    print(chalk.white('PRESS ENTER TO CONTINUE'));
    String? t0 = stdin.readLineSync();

    print("\x1B[2J\x1B[0;0H");
  }

  void showPlayerStat(Warrior player) {
    String? skillNum;
    print('${player.name}');
    print('HP : ${player.health}');
    print('DAMAGE : ${player.damage}');
    print('============================');
    print('${player.showSkill()} \n\n');
  }

  void showWinner(winner) {
    if (winner == player1) {
      print(
          '''

██████╗ ██╗      █████╗ ██╗   ██╗███████╗██████╗      ██╗    ██╗    ██╗██╗███╗   ██╗    ██╗
██╔══██╗██║     ██╔══██╗╚██╗ ██╔╝██╔════╝██╔══██╗    ███║    ██║    ██║██║████╗  ██║    ██║
██████╔╝██║     ███████║ ╚████╔╝ █████╗  ██████╔╝    ╚██║    ██║ █╗ ██║██║██╔██╗ ██║    ██║
██╔═══╝ ██║     ██╔══██║  ╚██╔╝  ██╔══╝  ██╔══██╗     ██║    ██║███╗██║██║██║╚██╗██║    ╚═╝
██║     ███████╗██║  ██║   ██║   ███████╗██║  ██║     ██║    ╚███╔███╔╝██║██║ ╚████║    ██╗
╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝     ╚═╝     ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝    ╚═╝
                                                                                           

 ''');
    } else if (winner == player2) {
      print(
          '''

██████╗ ██╗      █████╗ ██╗   ██╗███████╗██████╗     ██████╗     ██╗    ██╗██╗███╗   ██╗    ██╗
██╔══██╗██║     ██╔══██╗╚██╗ ██╔╝██╔════╝██╔══██╗    ╚════██╗    ██║    ██║██║████╗  ██║    ██║
██████╔╝██║     ███████║ ╚████╔╝ █████╗  ██████╔╝     █████╔╝    ██║ █╗ ██║██║██╔██╗ ██║    ██║
██╔═══╝ ██║     ██╔══██║  ╚██╔╝  ██╔══╝  ██╔══██╗    ██╔═══╝     ██║███╗██║██║██║╚██╗██║    ╚═╝
██║     ███████╗██║  ██║   ██║   ███████╗██║  ██║    ███████╗    ╚███╔███╔╝██║██║ ╚████║    ██╗
╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝    ╚══════╝     ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝    ╚═╝
                                                                                               

 ''');
    }
  }

  void Goodbye() {
    print("\x1B[2J\x1B[0;0H");
    print(
        '''

                             ██████╗  ██████╗  ██████╗ ██████╗ ██████╗ ██╗   ██╗███████╗
                            ██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝██╔════╝
                            ██║  ███╗██║   ██║██║   ██║██║  ██║██████╔╝ ╚████╔╝ █████╗  
                            ██║   ██║██║   ██║██║   ██║██║  ██║██╔══██╗  ╚██╔╝  ██╔══╝  
                            ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝██████╔╝   ██║   ███████╗
                             ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝ ╚═════╝    ╚═╝   ╚══════╝
                                                            

 ''');
  }
}

void main(List<String> arguments) {
  RunGame runGame = RunGame();
  runGame.mainMenu();
  if (runGame.enterGame() == true) {
    runGame.selectWarriorScreen();
    runGame.ingame();
  } else {
    runGame.Goodbye();
    return;
  }
}
