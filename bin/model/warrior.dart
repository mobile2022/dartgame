import '../skill/basicSkill.dart';

abstract class Warrior implements BasicSkill {
  String name;
  int damage;
  int health;
  int maxCountdown;
  List skillList = [];

  Warrior(
      this.name, this.damage, this.health, this.maxCountdown, this.skillList);
}
