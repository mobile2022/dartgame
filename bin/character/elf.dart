import '../model/warrior.dart';
import '../skill/magicalSkill.dart';

class Elf extends Warrior with MagicalSkill {
  Elf() : super('ELF', 30, 80, 12, ['1 Damage+5', '2 Heal 45']);
  // Elf(super.name, super.damage, super.health, super.maxCountdown);

  @override
  int attack() {
    return this.damage;
  }

  @override
  List showSkill() {
    return skillList;
  }

  @override
  int useSkill(String? number) {
    int skillDamage = damage;
    if (number == '') {
      return damage;
    } else if (number == '1') {
      skillDamage = plus5Damage(damage);
    } else if (number == '2') {
      return heal45();
    }
    return skillDamage;
  }
}

// final Elf elf = Elf('ELF', 30, 80, 12);
