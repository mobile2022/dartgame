import '../model/warrior.dart';
import '../skill/physicalSkill.dart';

class Archer extends Warrior with PhysicalSkill {
  Archer() : super('ARCHER', 25, 85, 7, ['1 Damage+10', '2 heal 10']);
  // Archer(super.name, super.damage, super.health, super.maxCountdown);

  @override
  void attack() {
    print('$name attack\n');
  }

  @override
  List showSkill() {
    return skillList;
  }

  @override
  int useSkill(String? number) {
    int skillDamage = damage;
    if (number == '') {
      return damage;
    } else if (number == '1') {
      skillDamage = plus10Damage(damage);
    } else if (number == '2') {
      return heal10();
    }
    return skillDamage;
  }
}

// final Archer archer = Archer('ARCHER', 25, 85, 7);
