import '../model/warrior.dart';
import '../skill/magicalSkill.dart';

class Wizard extends Warrior with MagicalSkill {
  Wizard() : super('WIZARD', 40, 50, 17, ['1 Damage+5', '2 Heal 45']);
  // Wizard(super.name, super.damage, super.health, super.maxCountdown);

  @override
  int attack() {
    return this.damage;
  }

  @override
  List showSkill() {
    return skillList;
  }

  @override
  int useSkill(String? number) {
    int skillDamage = damage;
    if (number == '') {
      return damage;
    } else if (number == '1') {
      skillDamage = plus5Damage(damage);
    } else if (number == '2') {
      return heal30();
    }
    return skillDamage;
  }
}

// final Wizard wizard = Wizard('WIZARD', 40, 50, 17);
