import '../model/warrior.dart';
import '../skill/physicalSkill.dart';

class Cowboy extends Warrior with PhysicalSkill {
  Cowboy() : super('COWBOY', 15, 100, 5, ['1 Double Damage', '2 heal 10']);
  // Cowboy(super.name, super.damage, super.health, super.maxCountdown);

  @override
  void attack() {
    print('$name attack\n');
  }

  @override
  List showSkill() {
    return skillList;
  }

  @override
  int useSkill(String? number) {
    int skillDamage = damage;
    if (number == '') {
      return damage;
    } else if (number == '1') {
      skillDamage = doubleDamage(damage);
    } else if (number == '2') {
      return heal10();
    }
    return skillDamage;
  }
}

// final Cowboy cowboy = Cowboy('COWBOY', 15, 100, 5);
